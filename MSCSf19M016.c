#include <unistd.h>
#include <stdio.h>
#include <dirent.h>
#include <string.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <errno.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>

extern int errno;//In case of error

///////////////////////without -l option ///////////////
void do_ls(char * dir)
{
   struct dirent * entry;
   DIR * dp = opendir(dir);
   if (dp == NULL){
      fprintf(stderr, "Cannot open directory:%s\n",dir);
      return;
   }
   errno = 0;
   while((entry = readdir(dp)) != NULL)
   {
         if(entry == NULL && errno != 0)
	 {
  		perror("readdir failed");
	 }	
 	 else
	 {
                if(entry->d_name[0] == '.')
		{
		}
		else
		{
			printf("%s\n",entry->d_name);
		}
		}
   }
   closedir(dp);
}
/////////////////////// Long listing ////////////////////////////
void long_listing(char * fname)
{
	struct stat file_info;
	int stat_return = stat(fname, &file_info);
	if(stat_return == -1)
	{
		printf("stat failed");
	}
	else
	{
		char permissions[11];
                strcpy(permissions, "----------");

		if ((file_info.st_mode &  0170000) == 0010000)
		{
			permissions[0] = 'p';
		}
		else if ((file_info.st_mode &  0170000) == 0020000)
		{
			permissions[0] = 'c';
		}
   		else if ((file_info.st_mode &  0170000) == 0040000)
		{
			permissions[0] = 'd';
		}
   		else if ((file_info.st_mode &  0170000) == 0060000)
		{
			permissions[0] = 'b';
		}
   		else if ((file_info.st_mode &  0170000) == 0100000)
		{
			permissions[0] = '-';
		}
   		else if ((file_info.st_mode &  0170000) == 0120000)
		{
			permissions[0] = '|';
		}
   		else if ((file_info.st_mode &  0170000) == 0140000)
		{
			permissions[0] = 's';
		}
		else
		{
			permissions[0] = '*';
		}

		//user
		if((file_info.st_mode & 0000400) == 0000400 )
		{
			permissions[1] = 'r';
		}
		if((file_info.st_mode & 0000200) == 0000200)
		{
			permissions[2] = 'w';
		}
		if((file_info.st_mode & 0000100) == 0000100)
		{
			permissions[3] = 'x';
		}
		//group
		if((file_info.st_mode & 0000040) == 0000040 )
                {
                        permissions[4] = 'r';
                }
                if((file_info.st_mode & 0000020) == 0000020)
                {
                        permissions[5] = 'w';
                }
                if((file_info.st_mode & 0000010) == 0000010)
                {
                        permissions[6] = 'x';
                }
		//others
		if((file_info.st_mode & 0000004) == 0000004 )
                {
                        permissions[7] = 'r';
                }
                if((file_info.st_mode & 0000002) == 0000002)
                {
                        permissions[8] = 'w';
                }
                if((file_info.st_mode & 0000001) == 0000001)
                {
                        permissions[9] = 'x';
                }

		printf("%s \t", permissions); 
		printf("%ld \t", file_info.st_nlink);

		struct passwd * passwordData = getpwuid(file_info.st_uid);
		printf("%s \t", passwordData->pw_name);

		struct group * groupData = getgrgid(file_info.st_gid);
		printf("%s \t", groupData->gr_name);
		printf("%ld \t", file_info.st_size);

		long seconds = file_info.st_atime;
		printf("%s \t", ctime(&seconds));

		if(permissions[0] == '-') 
		{
			//printf("\033[0;33m");
			printf("%s \t", fname);
			//printf("\033[0m");
		}
		if(permissions[0] == 'd')
                {
                        printf("\033[0;34m");
                        printf("%s \t", fname);
                        printf("\033[0m");
                }
		if(permissions[0] == 'l')
                {
                        printf("\033[0;35m");
                        printf("%s \t", fname);
                        printf("\033[0m");
                }
		if(permissions[0] == 'c')
                {
                        printf("\033[0;36m");
                        printf("%s \t", fname);
                        printf("\033[0m");
                }
		if(permissions[0] == 'b')
                {
                        printf("\033[0;37m");
                        printf("%s \t", fname);
                        printf("\033[0m");
                }

		printf("\n");
	}
}
/////////////////////main function ///////////////
int main(int argc, char* argv[]){
   	if (argc == 1)
   	{
      		do_ls(".");
   	}
   	else if(argc == 2)
   	{
      		int i=0;
      		while(++i < argc){
         		printf("Directory listing of %s:\n", argv[i] );
	      	do_ls(argv[i]);
      		}
   	}
   	else
   	{
		int i=1;
	   	while(++i < argc)
	   	{
         		struct dirent * entry;
        		DIR * dp = opendir(argv[i]);
        		if (dp == NULL){
                		fprintf(stderr, "Cannot open directory:%s\n",argv[i]);
        			return 0;
        		}
        		else
        		{
                		errno = 0;
                		while((entry = readdir(dp)) != NULL)
                		{
                        		if(entry == NULL && errno != 0)
                        		{
                                		perror("readdir failed");
                        		}
                        		else
                        		{
						if(entry->d_name[0] != '.')
						{
							long_listing(entry->d_name);
						}
                        		}
                		}
        		}		
	   	}
   	}
   	return 0;
}
